package main

import (
  "github.com/aws/aws-lambda-go/lambda"
  "github.com/asafschers/goscore"
  "encoding/xml"
  "io/ioutil"
  "github.com/aws/aws-lambda-go/events"
  "net/http"
  "log"
)

type Request struct {
  PassengerId int `json:"passengerid,string"`
  Age int `json:"age,string"`
  Sex string  `json:"sex"`
  Parch int `json:"parch,string"`
  Fare float64 `json:"fare,string"`
  Pclass  int `json:"pclass,string"`
  SibSp int `json:"sibsp,string"`
  Embarked  string `json:"embarked"`
}

func Handler(event Request) (events.APIGatewayProxyResponse, error) {

  log.Println(event)

  url := "https://s3-us-west-2.amazonaws.com/titanic-api/titanic_rf.pmml"
  resp, err := http.Get(url)
  if err != nil {
    return events.APIGatewayProxyResponse{}, err
  }
  defer resp.Body.Close()
  modelXml, err := ioutil.ReadAll(resp.Body)
  if err != nil {
    return events.APIGatewayProxyResponse{}, err
  }


  var model goscore.RandomForest
  xml.Unmarshal([]byte(modelXml), &model)
  featureSet := map[string]interface{}{
    "Sex": event.Sex,
    "Parch": event.Parch,
    "Age": event.Age,
    "Fare": event.Fare,
    "Pclass": event.Pclass,
    "SibSp": event.SibSp,
    "Embarked": event.Embarked,
    "PassengerId": event.PassengerId,
  }
  score, _ := model.LabelScores(featureSet)



  if score["0"] > score["1"] {
    return events.APIGatewayProxyResponse{
      Body: "Did not survive",
      StatusCode: http.StatusOK,
      Headers: map[string]string{"Content-Type": "application/json", "Access-Control-Allow-Origin":"*"},
    }, nil
  } else {
      return events.APIGatewayProxyResponse{
        Body: "Survived",
        StatusCode: http.StatusOK,
        Headers: map[string]string{"Content-Type": "application/json", "Access-Control-Allow-Origin":"*"},
      }, nil
  }

}

func main() {
  lambda.Start(Handler)
}
