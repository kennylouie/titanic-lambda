package main_test

import (
  "testing"
  main "gitlab.com/kennylouie/titanic-lambda"
)

func TestHandler(t *testing.T) {

  tests := []struct{
    request main.Request
    expect string
    err error
  }{
    {
      request: main.Request{
        Sex: "male",
        Parch: 3,
        Age: 26,
        Fare: 7.925,
        Pclass: 3,
        SibSp: 0,
        Embarked: "S",
        PassengerId: 3,
      },
      expect: "Did not survive",
      err: nil,
    },
    {
      request: main.Request{
        Sex: "female",
        Parch: 3,
        Age: 26,
        Fare: 7.925,
        Pclass: 3,
        SibSp: 0,
        Embarked: "S",
        PassengerId: 3,
      },
      expect: "Survived",
      err: nil,
    },
  }

  for _, test := range tests {
    response, err := main.Handler(test.request)
    if response.Body != test.expect {
      t.Fatalf("expected response to be %v, but got %v", test.expect, response.Body)
    }
    if err != test.err {
      t.Fatalf("expected err to be %v, but got %v", test.err, err)
    }
  }
}

