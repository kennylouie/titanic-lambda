# titanic-lambda

## Introduction

Blog post for the full description of this project is found at https://kennylouie.gitlab.io.

The same API previously built https://gitlab.com/kennylouie/titanic-api, but deployed severlessly on AWS lambda/API gateway. This API allows for API POST calls to query a machined trained model for the prediction of survivability of a titanic passenger. Based on the Kaggle Titanic dataset.

It is live and can be reached at this endpoint: https://9nfmilz74g.execute-api.us-west-2.amazonaws.com/Prod/

``` bash
curl -X POST -d '{"sex": "female", "parch": "0", "age": "26", "fare": "7.925", "pclass": "3", "sibsp": "0", "embarked": "S", "passengerid": "3"}' https://9nfmilz74g.execute-api.us-west-2.amazonaws.com/Prod/
```

## Dependencies

+ goscore
+ aws-lambda-go

``` bash
go get github.com/asafschers/goscore
go get github.com/aws/aws-lambda-go/lambda
go get github.com/aws/aws-lambda-go/events
```
